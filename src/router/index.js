import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
    {
      path: '/',
      name: 'Home',
      component: () => import('../views/Home')
    },
    // {
    //   path: '/about',
    //   name: 'About',
    //   // route level code-splitting
    //   // this generates a separate chunk (about.[hash].js) for this route
    //   // which is lazy-loaded when the route is visited.
    //   component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
    // }
    {
        path: '/capital-leter',
        name: 'CapitalLeter',
        component: () => import('../views/CapitalLeter')
    },
    {
        path: '/number-square',
        name: 'NumberSquare',
        component: () => import('../views/NumberSquare')
    },
    {
        path: '/wiki',
        name: 'Wiki',
        component: () => import('../views/Wiki')
    },
    {
        path: '/completer',
        name: 'Completer',
        component: () => import('../views/AutoCompleter')
    },
    {
        path: '/quiz',
        name: 'Quiz',
        component: () => import('../views/Quiz')
    },
    {
        path: '/lex-tale',
        name: 'LexTale',
        component: () => import('../views/LexTale')
    },
]

const router = new VueRouter({
    mode: 'history',
    routes
})

export default router
